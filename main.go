package main

import (
	"fmt"
	"strconv"
)

func main()  {
	// TODO : func main ini adalah function utama di dalam aplikasi golang, jadinya saat aplikasi golang dijalankan
	// TODO : func main akan dijalankan pertama kali. bisa disebut function inti

	a := 7
	b := 9

	d := hitung(b,a)
	fmt.Println("INI HASILNYA : ", d)
}


// TODO gue punya kerjaan banyak. gue cape ngerjainnya. otomatis gue minta tolong ke angga
// TODO si angga gue kasih tugas buat ngerjain
// TODO si angga udah selesai ngerjain tugas gue. otomatis lu ngasih hasil tugasnya ke gue lagi
func hitung (angga, aldian int) string {

	c := angga + aldian

	//var str string
	//str = "9"
	//
	//
	//res, _ := strconv.Atoi(str)

	// strconv.Itoa -> convert dari int to string
	d := strconv.Itoa(c)

	return d
}
